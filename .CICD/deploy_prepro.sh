#! /bin/bash
echo "Realizando el Despligue en el ambiente \"Pre-Production\""
gcloud auth activate-service-account $ACCOUNT_PREPRO --key-file=pre_pro_key.json --project=$PREPRO_ID > /dev/null 2>&1
gcloud config set core/project $PREPRO_ID > /dev/null 2>&1
VERSION=$(($(date +%s%N)/1000000))
sed -i 's,PROJECT_ID,'"$PREPRO_ID"',g' pom.xml
sed -i 's,VERSION_ISODATE,'"$VERSION"',g' pom.xml
sed -i 's,URL,'"$URL_PREPRO"',g' pom.xml
echo "EXPEDIENT=$PREPRO_DIGITALFILE" > src/main/resources/configurations.properties
MVN_ARTIFACTID=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.artifactId}' --non-recursive exec:exec) > /dev/null 2>&1
mvn clean package appengine:stage 
mvn appengine:deploy -Dapp.deploy.deployables=$(pwd)/src/main/appengine/app.yaml
bash postToSlack.sh -h "$SLACK_URL" -c sys^Cm-operations -u sysopsusr -i terminal -C 1974D2 -m "Se ha actualizado el servicio  $MVN_ARTIFACTID en el ambiente $PREPRO_ID con la URL: $URL_PREPRO" > /dev/null 2>&1
