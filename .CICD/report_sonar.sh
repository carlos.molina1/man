#!/usr/bin/env bash

export MVN_GROUPID=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.groupId}' --non-recursive exec:exec) > /dev/null 2>&1
export MVN_ARTIFACTID=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.artifactId}' --non-recursive exec:exec) > /dev/null 2>&1
echo "Gererando reporte de sonarqube del proyecto: ${MVN_ARTIFACTID}"
mvn clean package -Dmaven.test.skip=true > /dev/null 2>&1
mvn --batch-mode compile -Dmaven.test.skip=true -Djacoco.skip=true > /dev/null 2>&1
mvn --batch-mode verify org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar -Dsonar.host.url=http://104.198.29.6:9000 -Dsonar.login=37c0dd3db9aea1258f6ed042253513b524a5f445  -Dmaven.test.skip=true
echo "Reporte Finalizado"
REPORT_URL="http://104.198.29.6:9000/dashboard?id=${MVN_GROUPID}%3A${MVN_ARTIFACTID}"
curr_date=$(date +"%D a las %H:%M")
bash .CICD/postToSlack.sh -h "https://hooks.slack.com/services/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" -c sys^Cm-operations -u sysopsusr -i terminal -C 1974D2 -m " Se realizo el reporte de sonarqube del API: \"${MVN_ARTIFACTID}\" el día de $curr_date \n Puede consultarse en: \n ${REPORT_URL}"
