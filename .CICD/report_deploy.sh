#! /bin/bash

MVN_VERSION=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec) > /dev/null 2>&1
MVN_GROUPID=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.groupId}' --non-recursive exec:exec) > /dev/null 2>&1
MVN_ARTIFACTID=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.artifactId}' --non-recursive exec:exec) > /dev/null 2>&1
echo "<dependency>\n" > msg.txt 
echo "    <groupId>$MVN_GROUPID</groupId>\n" >> msg.txt
echo "    <artifactId>$MVN_ARTIFACTID</artifactId>\n" >> msg.txt
echo "    <version>$MVN_VERSION</version>\n" >> msg.txt
echo "</dependency>" >> msg.txt
LIB=$(cat msg.txt)
echo "Gererando reporte de la sonarqube del servicio: ${MVN_ARTIFACTID}"
mvn clean package
mvn --batch-mode compile -Dmaven.test.skip=true -Djacoco.skip=true > /dev/null 2>&1
mvn --batch-mode verify org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_LOGIN > /dev/null 2>&1
echo "Reporte Finalizado"
REPORT_URL="${SONAR_URL}/dashboard?id=${MVN_GROUPID}%3A${MVN_ARTIFACTID}"
bash postToSlack.sh -h "$SLACK_URL" -c sys^Cm-operations -u sysopsusr -i terminal -C 1974D2 -m " Se realizo el reporte de sonarqube del servicio: \"${MVN_ARTIFACTID}\" \n Puede consultarse en: \n ${REPORT_URL}" > /dev/null 2>&1
