#! /bin/bash
echo "Realizando el Despligue en el ambiente \"Test\""
gcloud auth activate-service-account $ACCOUNT_TEST --key-file=test_key.json --project=$TEST_ID > /dev/null 2>&1
gcloud config set core/project $TEST_ID > /dev/null 2>&1
VERSION=$(($(date +%s%N)/1000000))
MVN_ARTIFACTID=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.artifactId}' --non-recursive exec:exec)
echo "$URL_FOLIO_TEST"
sed -i 's,PROJECT_ID,'"$TEST_ID"',g' pom.xml
sed -i 's,VERSION_ISODATE,'"$VERSION"',g' pom.xml
sed -i 's,URL,'"$URL_TEST"',g' pom.xml
sed -i 's,URL_EXPEDIENT,'"$URL_EXPEDIENT_TEST"',g' configurations_replace.properties
# Varibles de entorno de app.yml
sed -i 's,URL_EXPEDIENT,'"$URL_EXPEDIENT_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_REGISTER,'"$URL_REGISTER_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_RENAPO,'"$URL_RENAPO_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_DGP,'"$URL_DGP_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_FOLIO,'"$URL_FOLIO_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_INE,'"$URL_INE_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_TSA,'"$URL_TSA_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_SESSION,'"$URL_SESSION_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_FS_RESOURCES,'"$URL_FS_RESOURCES_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_CACHE,'"$URL_CACHE_TEST"',g' src/main/appengine/app.yaml
#  Pendientes por checar si se usan en el proyecto
sed -i 's,URL_FIREBASE,'"$URL_FIREBASE_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_MONGO,'"$URL_MONGO_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_SEPOMEX,'"$URL_SEPOMEX_TEST"',g' src/main/appengine/app.yaml
sed -i 's,URL_SMS,'"$URL_SMS_TEST"',g' src/main/appengine/app.yaml
cp -f configurations_replace.properties src/main/resources/configurations.properties
# Variables de web.xml
sed -i 's,PRIV_CAPTCHA_KEY,'"$PRIVATE_CAPTCHA_KEY_TEST"',g' web_replace.xml
sed -i 's,PUB_CAPTCHA_KEY,'"$PUBLIC_CAPTCHA_KEY_TEST"',g' web_replace.xml
cp -f web_replace.xml src/main/webapp/WEB-INF/web.xml
# Variables de javascript
echo "$FIREBASE_KEY_TEST" > firebase.json
sed -i s#FIREBASE_CONFIG#"$(sed ':a;N;$!ba;s/\n/ /g' firebase.json)"#g script_replace.js
cp -f script_replace.js src/main/webapp/resources/js/scripts.js
#echo "\nCONFIGURATIONS\n"
#cat src/main/resources/configurations.properties
#echo "\nPOM\n"
#cat pom.xml
#echo "\nYAML\n"
#cat src/main/appengine/app.yaml
#echo "\nSCRIPTS\n"
#cat src/main/webapp/resources/js/scripts.js
#echo "\nWEB\n"
#cat src/main/webapp/WEB-INF/web.xml
mvn clean package appengine:stage
mvn appengine:deploy -Dapp.deploy.deployables=$(pwd)/src/main/appengine/app.yaml
bash postToSlack.sh -h "$SLACK_URL" -c sys^Cm-operations -u sysopsusr -i terminal -C 1974D2 -m "Se ha actualizado el servicio $MVN_ARTIFACTID en el ambiente $TEST_ID con la URL: $URL_TEST"
