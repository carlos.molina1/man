#!/bin/bash

VERSIONS=$(gcloud app versions list --service $1 --sort-by '~version' --format 'value(version.id)')
COUNT=0
echo "Guardando $2 versiones de la aplicación $1"
for VERSION in $VERSIONS
do
    ((COUNT++))
    if [ $COUNT -gt $2 ]
    then
      echo "Borrando la version $VERSION de la aplicación $1"
      gcloud app versions delete $VERSION --service $1 -q
    else
      echo "Registrando la version $VERSION de la aplicación $1"
    fi
done