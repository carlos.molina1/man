// Initialize Firebase
var config = FIREBASE_CONFIG;
firebase.initializeApp(config);

var auth = firebase.auth();

function login(email, password) {
    auth.signInWithEmailAndPassword(email, password).then(function (r) {
        if (r.user.emailVerified)
            verifyUid(r.user.refreshToken);
        else
            primeMessage('Error', 'Es necesario que verifiques tu cuenta de correo');
    }).catch(function (e) {
        primeMessage('Error', 'Usuario y/o Contraseña incorrectos');
    });
}

function primeMessage(title, content, type) {
    if (!type)
        type = 'info';
    PF('growl').removeAll();
    PF('growl').renderMessage({
        "summary": title,
        "detail": content,
        "severity": type
    });
}

function verifyUid(token) {
    getSession([{name: "token", value: token}]);
}