package mx.gob.tfja.sjl.constants;

import java.util.HashMap;
import java.util.Map;

public class ConstantsFile {
    private ConstantsFile() {
        throw new IllegalStateException("ConstantsFile class");
    }

    public static final String ICON_FOLDER = "fa fa-folder";
    public static final String ICON_PDF = "fa fa-file-pdf-o";
    public static final String ICON_MUSIC = "fa fa-music";
    public static final String ICON_FILM = "fa fa-film";
    public static final String ICON_WORD = "fa fa-file-word-o";
    public static final String ICON_PICTURE = "fa fa-file-picture-o";
    public static final String ICON_EXCEL = "fa fa-file-excel-or";
    public static final String ICON_TXT = "fa fa-file-o";
    public static final String ICON_CSV = "fa fa-file-text";
    public static final String ICON_CODE = "fa fa-file-code-o";
    public static final String ICON_POWERPOINT = "fa fa-file-powerpoint-o";
    public static final String ICON_ZIP = "fa fa-file-zip-o";
    public static final String ICON_START_PROCESS = "fa fa-caret-square-o-right";
    public static final String ICON_CANCEL = "fa fa-times";
    public static final String ICON_COMMENTS = "fa fa-comments";
    public static final String ICON_DETAIL = "fa fa-info-circle";
    public static final String ICON_FILE = "fa fa-file";

    public static final String ICON_GLOBE = "fa fa-globe";
    public static final String ICON_LOCK = "fa fa-lock";
    public static final String ICON_EYE = "fa fa-eye";

    private static final Map<String, String> ICONS;
    static {
        ICONS = new HashMap<>();
        ICONS.put("folder", ICON_FOLDER);
        ICONS.put("pdf", ICON_PDF);
        ICONS.put("mp3", ICON_MUSIC);
        ICONS.put("mp4", ICON_FILM);
        ICONS.put("odt", ICON_WORD);
        ICONS.put("doc", ICON_WORD);
        ICONS.put("png", ICON_PICTURE);
        ICONS.put("jpg", ICON_PICTURE);
        ICONS.put("ods", ICON_EXCEL);
        ICONS.put("xls", ICON_EXCEL);
        ICONS.put("txt", ICON_TXT);
        ICONS.put("csv", ICON_CSV);
        ICONS.put("xml", ICON_CODE);
        ICONS.put("java", ICON_CODE);
        ICONS.put("xhtml", ICON_CODE);
        ICONS.put("js", ICON_CODE);
        ICONS.put("ppt", ICON_POWERPOINT);
        ICONS.put("pptx", ICON_POWERPOINT);
        ICONS.put("zip", ICON_ZIP);
        ICONS.put("rar", ICON_ZIP);
    }

    public static final String PROPERTIESDOCSCLASS = "documentClassification";

    public static String getICONS(String key) {
        return ICONS.get(key);
    }

    public static String getOrDefault(String key, String defaultValue)
    {
        return ICONS.getOrDefault(key, defaultValue);
    }


}
