package mx.gob.tfja.sjl.constants;

public class Numeros {

    public static final int CERO = 0;
    public static final int UNO = 1;
    public static final int DOS = 2;
    public static final int CIEN = 100;
}
