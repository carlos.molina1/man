package mx.gob.tfja.sjl.constants;

public class ConstantsUtil {
    private ConstantsUtil() {
        throw new IllegalStateException("ConstantsUtil class");
    }

    public static final String TOKEN = "token";
    public static final String CODE_S_ERROR_S = "code %s error %s";
    public static final String CAUSE_S_ERROR_S = "cause %s error %s";
}
