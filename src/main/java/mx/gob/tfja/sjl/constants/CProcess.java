package mx.gob.tfja.sjl.constants;

public class CProcess {

    public static final String PROCESO_IN_CACHE = "procesoCache";
    public static final String PROPERTY_NUMBER="number";
    public static final String PROPERTY_URL="url";
    public static final String PROPERTY_TITLE="title";
    public static final String PROPERTY_COLOR="color";


    public static final String COLOR_ACTIVE="#E5AA17";
    public static final String COLOR_PAST="#AAAAAA";
    public static final String COLOR_NEXT="#1976D2";

}
