package mx.gob.tfja.sjl.data;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import mx.gob.tfja.sjl.data.CreateDataSource;
import mx.gob.tfja.sjl.util.CheckSum512;
import net.sourceforge.jtds.jdbc.JtdsConnection;

public class InsertAcuDem {
    private static JtdsConnection cn;
    public  void addDem(){
        try{
            JtdsConnection cn=CreateDataSource.getConnection();
            Statement stm = cn.createStatement();
            ResultSet rst = stm.executeQuery("Select MAX(num_entrada) from dbo.GENM_EXPEDIENTES\n" +
                    "where ejercicio = YEAR(GETDATE())");
            if (rst.next()) {
                System.out.println(rst.getString(1));

                String strNumEntrada = rst.getString(1);

                String sqlCheck = "Select CHECKSUM_AGG(BINARY_CHECKSUM(*)) from GENM_EXPEDIENTES where num_entrada = " + strNumEntrada +
                      " AND ejercicio = YEAR(GETDATE())";

                System.out.println(sqlCheck);

                //JtdsConnection cn2=CreateDataSource.getConnection();
                //Statement stm = cn.createStatement();
                ResultSet rstCheck = stm.executeQuery("Select CHECKSUM_AGG(BINARY_CHECKSUM(*)) from GENM_EXPEDIENTES where num_entrada = " + strNumEntrada +
                        " AND ejercicio = YEAR(GETDATE())");
                if(rstCheck.next()) {
                    System.out.println(rstCheck.getString(1));
                }

                String strCheckSum = CheckSum512.encryptThisString(rstCheck.getString(1));
                String sqlCommand = "INSERT INTO GENM_ACUSES (num_expediente,cadena,cadena_imp,llave_privada) VALUES ('" + rst.getString(4) + "','" + strCheckSum + "','" +  strCheckSum  + "','" + "'LLAVE PRIV')";
                stm.executeQuery(sqlCommand);


            }else{
                System.out.println("No se encontraron expedientes para el ejercicio actual");
            }

        }
        catch(SQLException e){
            e.getMessage();
        }


    }

}
