package mx.gob.tfja.sjl.data;


import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jmolina
 */
public class SqlDataSource {

    //public static  List<Map<String, String>> regionList;
    public static List<Map<String, Object>> regionList;


    public static DataSource mSQLdataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

        //dataSource.setUrl("jdbc:sqlserver://192.168.34.46:1433/BDSICJ");
        dataSource.setUrl("jdbc:sqlserver://192.168.34.46:1433;databaseName=BDSICJ");
        dataSource.setUsername("entsis");
        dataSource.setPassword("entsis");

        return dataSource;
    }

    public static void main(String[] args) throws SQLException {
        try {

//            JtdsConnection cn = CreateDataSource.getConnection();
            JdbcTemplate jdbc = new JdbcTemplate(mSQLdataSource());
            System.out.println(jdbc.toString());
//            List<ClacGates> gates = jdbc.query("select * from CLAC_SALAS", new Object[]{}, new ClacGates());
//            System.out.println("gates: " + gates);
            //regionList = jdbc.queryForList("select region from CLAC_SALAS", new Object[]{}, String.class);

            String sql = "Select MAX(num_entrada) from dbo.GENM_EXPEDIENTES" +
                                        "where ejercicio = YEAR(GETDATE())";
            regionList = (List<Map<String, Object>>) jdbc.queryForList(sql);
            regionList.forEach(System.out::println);


        } catch (Exception e) {
            System.out.println(e);
        }
        //return regionList;
    }


}