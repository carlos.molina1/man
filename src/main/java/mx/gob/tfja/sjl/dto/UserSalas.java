package mx.gob.tfja.sjl.dto;

public class UserSalas {
    private int id;
    private String cveUser;
    private  int cveRegion, cveSala, cveMag, cveSrio;


    public UserSalas(){
    }

    public UserSalas(int id,String cveUser, int cveRegion, int cveSala, int cveMag, int cveSrio) {
        this.id = id;
        this.cveUser = cveUser;
        this.cveRegion = cveRegion;
        this.cveSala = cveSala;
        this.cveMag = cveMag;
        this.cveSrio = cveSrio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCveUser() {
        return cveUser;
    }

    public void setCveUser(String cveUser) {
        this.cveUser = cveUser;
    }

    public int getCveRegion() {
        return cveRegion;
    }

    public void setCveRegion(int cveRegion) {
        this.cveRegion = cveRegion;
    }

    public int getCveSala() {
        return cveSala;
    }

    public void setCveSala(int cveSala) {
        this.cveSala = cveSala;
    }

    public int getCveMag() {
        return cveMag;
    }

    public void setCveMag(int cveMag) {
        this.cveMag = cveMag;
    }

    public int getCveSrio() {
        return cveSrio;
    }

    public void setCveSrio(int cveSrio) {
        this.cveSrio = cveSrio;
    }

    @Override
    public String toString() {
        return "UserSalas{" +
                "id=" + id +
                ", cveUser='" + cveUser + '\'' +
                ", cveRegion=" + cveRegion +
                ", cveSala=" + cveSala +
                ", cveMag=" + cveMag +
                ", cveSrio=" + cveSrio +
                '}';
    }
}
