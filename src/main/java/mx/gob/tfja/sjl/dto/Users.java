package mx.gob.tfja.sjl.dto;

public class Users {
    private String key, firstName, lastName, surname;


    public Users() {
    }

    public Users(String key, String lastName, String surname, String firstName) {
        this.key = key;
        this.firstName = firstName;
        this.lastName = lastName;
        this.surname = surname;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFirstName() {
        return firstName != null ? firstName.trim() : firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName != null ? lastName.trim() : lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurname() {
        return surname != null ? surname.trim() : surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


}
