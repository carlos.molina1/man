package mx.gob.tfja.sjl.util;

import mx.kreintosoft.data.ObjectData;
import org.primefaces.PrimeFaces;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class UtilsPrimefaces {
    private UtilsPrimefaces() {
        throw new IllegalStateException("UtilsPrimefaces class");
    }

    /**
     * Crea un mensaje en pantalla
     *
     * @param id
     * @param summary
     * @param message
     */
    public static void createMessage(String id, String summary, String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(id, new FacesMessage(summary, message));
    }

    /**
     * Busca un componente por su id en el formulario
     *
     * @param id    identificador de componente
     * @param where componente el donde se buscara
     * @return UIComponent Primefaces
     */
    public static UIComponent searchComponent(String id, UIComponent where) {
        if (where == null) {
            return null;
        } else if (where.getId().equals(id)) {
            return where;
        } else {
            List<UIComponent> childrenList = where.getChildren();
            if (childrenList == null || childrenList.isEmpty()) {
                return null;
            }
            for (UIComponent child : childrenList) {
                UIComponent result = null;
                result = searchComponent(id, child);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    /**
     * Función para crear un mensaje en un campo
     *
     * @param id      identificador del campo
     * @param summary Detalle error
     * @param message Mensaje de error
     */
    public static void createMsgInputs(String id, FacesContext context, FacesMessage.Severity severity, String summary, String message) {
        context.addMessage(id, new FacesMessage(severity, summary, message));
        context.renderResponse();
    }

    public static void createMessageErrorInput(ObjectData obj, FacesContext context, String idCmpRender) {
        String cmp = "";
        if (idCmpRender != null) {
            cmp = idCmpRender + obj.get("property").toString();
        } else {
            cmp = obj.get("property").toString();
        }
        UIComponent component = searchComponent(cmp, FacesContext.getCurrentInstance().getViewRoot());
        if (component != null) {
            String id = component.getClientId();
            if (id != null) {
                createMsgInputs(id, context, FacesMessage.SEVERITY_ERROR, "", obj.get("message").toString());
            }
        }
    }

    public static void createMsgErrorJsonSchema(ObjectData obj) {
        String cmp = obj.getString("key");
        UIComponent component = searchComponent(cmp, FacesContext.getCurrentInstance().getViewRoot());
        if (component != null) {
            String id = component.getClientId();
            if (id != null) {
                createMsgInputs(id, FacesContext.getCurrentInstance(), FacesMessage.SEVERITY_ERROR, "", obj.getString("value"));
            }
        }
    }

    public static String getMessageResource(String key, Object[] arguments, String messagesBaseName) {
        Locale locale = new Locale("es", "ES", "");
        FacesContext context = FacesContext.getCurrentInstance();
        if (context == null) {
            locale = Locale.getDefault();
        } else {
            if (context.getViewRoot() != null) {
                locale = context.getViewRoot().getLocale();
            }
        }
        String resourceString;
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(messagesBaseName, locale);
            resourceString = bundle.getString(key);
        } catch (MissingResourceException e) {
            return key;
        }
        if (arguments == null) {
            return resourceString;
        }
        MessageFormat format = new MessageFormat(resourceString, locale);
        return format.format(arguments);
    }

    public static void showDialog(String dialogName, Boolean showDialog){
        String statement = "PF('"+dialogName+"')."+
                (showDialog? "show":"hide")
                +"();";
        executeJQuery(statement);
    }

    private static void addMessage(FacesMessage.Severity severity, String summaryMessage, String detail) {
        FacesMessage message = new FacesMessage(severity, summaryMessage, detail);
        PrimeFaces.current().dialog().showMessageDynamic(message);
    }

    public static void addMessageInfo(String summaryMessage, String detail) {
        addMessage(FacesMessage.SEVERITY_INFO, summaryMessage, detail);
    }

    public static void addMessageInfo(String detail) {
        addMessage(FacesMessage.SEVERITY_INFO, "Info", detail);
    }

    public static void addMessaggeError(String detail) {
        addMessage(FacesMessage.SEVERITY_ERROR, "Error!", detail);
    }

    public static void executeJQuery(String statement) {
        PrimeFaces.current().executeScript(statement);
    }

    public static void executeUpdatePF(String... componentId) {
        PrimeFaces.current().ajax().update(componentId);
    }
}
