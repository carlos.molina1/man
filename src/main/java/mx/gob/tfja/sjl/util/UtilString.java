package mx.gob.tfja.sjl.util;

import org.apache.commons.codec.binary.Base64;
import java.text.Normalizer;

public class UtilString {

    public static final String EMPTY_STRING = "";
    public static final String COMA_STRING = ",";
    private static final String REMOVE_SPECIALS_CHARACTERS= "\\p{InCombiningDiacriticalMarks}+" ;

    private UtilString() {
        throw new IllegalStateException("UtilString class");
    }

    public static String getReducedName(String name, String middleName, Integer numCaracteres){
        String nameTmp = name + " " + middleName;
        if (nameTmp.length() > numCaracteres) {
            nameTmp = nameTmp.substring(0, numCaracteres);
            int pos = nameTmp.lastIndexOf(' ');
            if (pos != -1) {
                return nameTmp.substring(0, pos) + " ...";
            } else {
                return nameTmp + "...";
            }
        }
        return nameTmp;
    }
    /**
     * Method that transform the input string into a 64 base String
     * @param input String to transform
     * @return String in 64 base
     */
    public static String cifrarBase64(String input) {
        byte[] encodedBytes = Base64.encodeBase64(input.getBytes());
        return Base64.encodeBase64String(encodedBytes);
    }

    /**
     * Method that untransform the input string into a 64 base String
     * @param input String to untransform
     * @return String in 64 base
     */
    public static String descifrarBase64(String input) {
        byte[] decodedBytes = Base64.decodeBase64(input);
        return new String(Base64.decodeBase64(decodedBytes));
    }

    /**
     * Method to validate if a String is null or empty
     * @param toValidate String to validate
     * @return True if is null or empty, False if is different
     */
    public static boolean isBlanckOrNull(String toValidate){
        return toValidate == null ||   UtilString.EMPTY_STRING.equals(toValidate);
    }

    /**
     * Method to know if a String contains an subString despite the accent and the lower or upper case.
     * @param stringContainer Base string
     * @param subString subString to find in the container string
     * @return True if the string container has the sub string
     */
    public static boolean stringContainString(String stringContainer, String subString){
        String normalized = Normalizer.normalize(subString, Normalizer.Form.NFD);
        String accentRemoved = normalized.replaceAll(UtilString.REMOVE_SPECIALS_CHARACTERS, UtilString.EMPTY_STRING);
        return stringContainer.toLowerCase().contains(accentRemoved.toLowerCase());
    }
}
