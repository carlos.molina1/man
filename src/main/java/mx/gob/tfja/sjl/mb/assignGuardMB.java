package mx.gob.tfja.sjl.mb;

import mx.gob.tfja.sjl.util.SessionUtils;
import net.sourceforge.jtds.jdbc.JtdsConnection;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

import mx.gob.tfja.sjl.data.CreateDataSource;
import mx.gob.tfja.sjl.data.SqlDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;

@ManagedBean(name = "assignGuardMB")
@ViewScoped
public class assignGuardMB {

    private JtdsConnection cn,cnRegion;
    private String regionDescription;
    private String region;
    private String usrMag;
    private String usrSrio;
    private String usrAct;
    private String usrArc;
    private String date1;
    private String date2;


    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }



    public String getUsrSrio() {
        return usrSrio;
    }

    public void setUsrSrio(String usrSrio) {
        this.usrSrio = usrSrio;
    }

    public String getUsrAct() {
        return usrAct;
    }

    public void setUsrAct(String usrAct) {
        this.usrAct = usrAct;
    }

    public String getUsrArc() {
        return usrArc;
    }

    public void setUsrArc(String usrArc) {
        this.usrArc = usrArc;
    }

    public String getUsrOfp() {
        return usrOfp;
    }

    public void setUsrOfp(String usrOfp) {
        this.usrOfp = usrOfp;
    }

    private String usrOfp;

    private String pass;
    private String base;
    //private static JtdsConnection cnRegion;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }



    public String getusrMag() {
        return usrMag;
    }

    public void setusrMag(String usrMag) {
        this.usrMag = usrMag;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public List<String> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<String> usersList) {
        this.usersList = usersList;
    }

    private List<String> usersList;


    //public RegDemMB() {
//        usersList = new HashMap<String, String>();
    //  }

    //public String getRegionDescription() {
    //  return regionDescription;
    //}
    public void setRegionDescription(String regionDescription) {
        this.regionDescription = regionDescription;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }


    @PostConstruct
    public void init() {
        HttpSession session = SessionUtils.getSession();
        //if (session.getAttribute("username") != null && session.getAttribute("username")!= "") {
        if (1 == 1) {
            cnRegion = (JtdsConnection) session.getAttribute("regConnection");

            //System.out.println("initialCn PostConstruct: " + initialCn);
            getUsers();
        }
        else{
            return;
        }

    }


    public void getUsers() {
        try {
            usersList= new ArrayList<>();

            //if (cnRegion != null) {
            if (1==1) {
                System.out.println("Conexión regional Lista !!!");
                Statement stm = cnRegion.createStatement();
                System.out.println("stm: "+stm);
                ResultSet rst = stm.executeQuery("Select cve_usuario, nombre, ap_paterno, ap_materno, RTRIM(nombre) + ' ' +  RTRIM(ap_paterno) + ' ' + RTRIM(ap_materno) + '|' + cve_usuario   As USER_NOM\n" +
                        " FROM ADMM_USUARIOS");
                //ResultSet rst = stm.executeQuery("Select RTRIM(descripcion) from CLAC_SALAS");

                while (rst.next()) {
                    usersList.add( rst.getString(5));
                }
            } else {
                System.out.println("Desconectado");
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("usersList: "+usersList.size());
    }

    //***************************************************************************************************************

    public void ejecutaPeriodGuard(){
        try {

            //String fecIniVac;
            //String fecFinVac;

            Statement stm = cnRegion.createStatement();
            System.out.println("Antes de ejecutar guardias");
            System.out.println("Fecha de Inicio de guardia" + date1 );
            System.out.println("Fecha de Inicio de guardia" + date2 );

            String myDatePattern1 = "dd/MM/yyyy";

            SimpleDateFormat df = new SimpleDateFormat( myDatePattern1 );
            System.out.println( "Fecha de Inicio: " + date1 );
            System.out.println( "Fecha de Fin: " + date2 );

            //String sDate1="31/12/1998";
            Date fecIniVac = new SimpleDateFormat("dd/MM/yyyy").parse(date1);
            Date fecFinVac = new SimpleDateFormat("dd/MM/yyyy").parse(date2);

            //SimpleDateFormat.parse

            //ResultSet rst = stm.executeQuery("exe sp_guardias_admon_nac '" + usrMag + "','" + usrSrio + "','" + usrAct + "','" + usrArc + "','" + fecIniVac + "','" + fecFinVac + "'" );
        }
        catch(Exception e){
              e.getMessage();
              e.getCause();
        }
    }



    public String redirectionTFJA(int xPage){
        //System.out.println("Por redireccionar");
        if (xPage == 0) {
            return "menuCentral";
        }
        if (xPage == 1) {
            return "changePass";
        }
        if (xPage == 2) {
            return "startCounter";
        }
        return "startCounter";
    }


}



