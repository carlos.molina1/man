package mx.gob.tfja.sjl.mb;

import mx.gob.tfja.sjl.data.CreateDataSource;
import mx.gob.tfja.sjl.dto.UserSalas;
import mx.gob.tfja.sjl.dto.Users;
import mx.gob.tfja.sjl.util.SessionUtils;
import net.sourceforge.jtds.jdbc.JtdsConnection;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



@ManagedBean(name = "usersPermissionsMB")
@SessionScoped

public class UsersPermissionsMB implements Serializable {
    private static JtdsConnection connection;
    private List<Users> users;
    private List<String> modules,allModules;
    private List<UserSalas> userGates;
private Users user;
    @PostConstruct
    public void init() {
        try {
            System.out.println("init");
            HttpSession session = SessionUtils.getSession();

            connection = (JtdsConnection) session.getAttribute("regConnection");
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("Select cve_usuario,ap_paterno,ap_materno,nombre from ADMM_USUARIOS");
            users = new ArrayList<>();
            //String key,String lastName, String surname, String firstName )
            while (rs.next()) {
                users.add(new Users(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
public void onUserSelect(){

        System.out.println("user: "+user);
        try {
            HttpSession session = SessionUtils.getSession();
            connection = (JtdsConnection) session.getAttribute("regConnection");
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("Select LTRIM(RTRIM(cve_modulo))from ADMR_USUARIOS_MODULOS where cve_usuario = '"+user.getKey()+"'");
            modules = new ArrayList<>();
            //String key,String lastName, String surname, String firstName )
            while (rs.next()) {
                modules.add(rs.getString(1));
            }
            System.out.println("modules: "+modules.size());
            statement = connection.createStatement();
            rs = statement.executeQuery("Select LTRIM(RTRIM(cve_modulo)) from ADMM_MODULOS");
            allModules = new ArrayList<>();
            //String key,String lastName, String surname, String firstName )
            while (rs.next()) {
                allModules.add(rs.getString(1));
            }
            System.out.println("allModules: "+allModules.size());

            //
        }catch (Exception e){
            e.printStackTrace();
        }


}
public void saveChanges(){
        try {

            HttpSession session = SessionUtils.getSession();
           /* connection = (JtdsConnection) session.getAttribute("regConnection");
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("Select LTRIM(RTRIM(cve_modulo)) from ADMR_USUARIOS_MODULOS where cve_usuario = '"+user.getKey()+"'");
            List<String> modulesOriginal = new ArrayList<>();
            //String key,String lastName, String surname, String firstName )
            while (rs.next()) {
                modulesOriginal.add(new StringBuilder("'").append(rs.getString(1)).append("'").toString());
            }*/
            // delete from ADMR_USUARIOS_MODULOS where cve_usuario = '"+user.getKey()+"'"
            // insert into
            /*

            INSERT INTO BDSICJ.dbo.ADMR_USUARIOS_MODULOS (cve_usuario, cve_modulo, modificar, restrictiva) Select user.getKey(),cve_modulo,0,0 from BDSICJ.dbo.ADMM_MODULOS where
            cve_modulo in (modules)

             */

            System.out.println("modules: "+modules.size());
            System.out.println("allModules: "+allModules.size());

            List<String> modulesL= new ArrayList<>(modules.size());
            modules.forEach(s->{
                modulesL.add(new StringBuilder("'").append(s.trim()).append("'").toString());
            });
            System.out.println("modulesL: "+modulesL);
            System.out.println("===========================");


            try {
                StringBuilder sb= new StringBuilder("");
                Statement statement = connection.createStatement();

                boolean b = statement.execute(" BEGIN TRAN MAN_MOD_1_V5_ALFA");

                sb.append(" \n ");
                sb.append(" delete from ADMR_USUARIOS_MODULOS where cve_usuario = '").append(user.getKey().trim()).append("'");
                sb.append(" \n ");
                sb.append(" INSERT INTO ADMR_USUARIOS_MODULOS (cve_usuario, cve_modulo, modificar, restrictiva) ");
                sb.append(" \n ");
                sb.append(" Select '").append(user.getKey().trim()).append("',cve_modulo,0,0 from ADMM_MODULOS where  cve_modulo in (");
                String modulesS=modulesL.toString();
                modulesS=modulesS.substring(1,modulesS.length()-1);
                System.out.println("modulesS: "+modulesS);
                sb.append(modulesS);
                sb.append(")");

                //sb.append(" \n ").append(" COMMIT TRAN T1 ").append(" \n ").append(" ROLLBACK TRAN T1 ");
                System.out.println("query: "+sb.toString());
                connection = (JtdsConnection) session.getAttribute("regConnection");



                statement = connection.createStatement();
                b = statement.execute(sb.toString());
                System.out.println("b " + b);

                statement = connection.createStatement();
                b = statement.execute(" COMMIT TRAN MAN_MOD_1_V5_ALFA ");

                System.out.println("Se realizó el cambio de módulos satisfactoriamente ");
                System.out.println("b " + b);


                //boolean b = statement.execute(" BEGIN TRAN T1 ");

                successMessage("¡ Módulos asignados/modificados satisfactoriamente ! ", "Asignación/Cambio Módulos");


            }
            catch (Exception e){
                Statement statement = connection.createStatement();
                boolean b = statement.execute(" ROLLBACK TRAN MAN_MOD_1_V5_ALFA ");
                System.out.println("Ocurrió un error al intentar cambiar los módulos ");
                errorMessage("Error en la asignación de Módulos " + e.getMessage() + " \n ","Error Asignación/Modificación Módulos");
                System.out.println("Error en la asignación de Módulos " + e.getMessage() + " \n ");
                return;
            }


        }catch (Exception e){
            e.printStackTrace();
        }

}

public void editUserData(){
    try {

        HttpSession session = SessionUtils.getSession();


        try {
            /*
            ,,
             */
            StringBuilder sb= new StringBuilder("update ADMM_USUARIOS set ap_paterno='").append(user.getLastName().trim()).append("',")
                    .append("ap_materno='").append(user.getSurname().trim()).append("',nombre='").append(user.getFirstName().trim()).append("'")
                    .append(" where cve_usuario='").append(user.getKey().trim()).append("'");
            Statement statement = connection.createStatement();

            boolean b = statement.execute(" BEGIN TRAN MAN_MOD_1_V5_ALFA");



            //sb.append(" \n ").append(" COMMIT TRAN T1 ").append(" \n ").append(" ROLLBACK TRAN T1 ");
            System.out.println("query: "+sb.toString());
            connection = (JtdsConnection) session.getAttribute("regConnection");



            statement = connection.createStatement();
            b = statement.execute(sb.toString());
            System.out.println("b " + b);

            statement = connection.createStatement();
            b = statement.execute(" COMMIT TRAN MAN_MOD_1_V5_ALFA ");

            System.out.println("Se realizó la edición satisfactoriamente ");
            System.out.println("b " + b);


            //boolean b = statement.execute(" BEGIN TRAN T1 ");

            successMessage("¡ Datos modificados satisfactoriamente ! ", "Edición de datos personales");


        }
        catch (Exception e){
            Statement statement = connection.createStatement();
            boolean b = statement.execute(" ROLLBACK TRAN MAN_MOD_1_V5_ALFA ");
            System.out.println("Ocurrió un error al intentar editar los datos del usuario");
            errorMessage("Error en editar los datos del usuario " + e.getMessage() + " \n ","Error Modificación de datos del usuario");
            System.out.println("Error en editar los datos del usuario " + e.getMessage() + " \n ");
            return;
        }


    }catch (Exception e){
        e.printStackTrace();
    }

}
    public void successMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,summary,message ) );
    }
    public void errorMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,summary,message ) );
    }
    public void warnMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,summary,message ) );
    }





    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public List<String> getAllModules() {
        return allModules;
    }

    public void setAllModules(List<String> allModules) {
        this.allModules = allModules;
    }
}
