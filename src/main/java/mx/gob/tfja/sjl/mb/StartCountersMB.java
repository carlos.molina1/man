package mx.gob.tfja.sjl.mb;

import mx.gob.tfja.sjl.data.CreateDataSource;
import net.sourceforge.jtds.jdbc.JtdsConnection;
import org.primefaces.PrimeFaces;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



@ManagedBean(name = "startCount")
@ViewScoped
public class StartCountersMB implements Serializable {

        private static JtdsConnection cn;
        private static JtdsConnection cnDest;

        private static JtdsConnection cnInit;

        private static JtdsConnection cnAdmin;
        private static Statement stmAdmin;
        private String usr;
        private String pass;
        private String base;
        private String region;
        private String bd;
        private String ip;
        private String port;
        private String usrSICSEJ;
        private String passUsrNew;
        public String[] arrOfStr;
        public int newUserCreated = 0;
        public int userExist = 0;
        public int loginExist = 0;


        public String getUsrSICSEJ() {
            return usrSICSEJ;
        }

        public void setUsrSICSEJ(String usrSICSEJ) {
            this.usrSICSEJ = usrSICSEJ;
        }

        public String getPassUsrNew() {
            return passUsrNew;
        }

        public void setPassUsrNew(String passUsrNew) {
            this.passUsrNew = passUsrNew;
        }





        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getBd() {
            return bd;
        }

        public void setBd(String bd) {
            this.bd = bd;
        }



    public void cnSearch()
    {
        try {

            if (usr.toUpperCase().indexOf("ADMIN") == -1 && usr.toUpperCase().indexOf("ADM") == -1 ){
                errorMessage("¡Error!","Solo un usuario administrador puede cambiar contraseñas");
                return;
            }
            if (usrSICSEJ.toUpperCase().indexOf("VISITA") >= 0){
                errorMessage("¡Error!","No puede cambiar la contraseña a usuarios de VISITA");
                return;
            }

            cn = CreateDataSource.getConnection();
            Statement stm = cn.createStatement();


            System.out.println("stm: "+stm);
            System.out.println("region: "+region);
            if(region!=null&&!region.trim().isEmpty()){
                //ResultSet rst = stm.executeQuery("select ip_server,nom_bd from CLAC_SALAS where descripcion = '" + region + "'");
                //CAT_BD_REGION_CLOUD
                arrOfStr = region.split("-",3);

                ResultSet rst = stm.executeQuery("select ref_sec,dns,ip_server_port from CLAC_SALAS where descripcion = '" + arrOfStr[0] + "'");

                while (rst.next()) {
                    ip = rst.getString(2);
                    bd = rst.getString(1);
                    port = rst.getString(3);
                    //usr = rst.getString(4);
                    //pass = rst.getString(5);
                }
                String username,password, sqlInstruction;  //inputs

                //validar que exite el usuario y contraseña con ip, bd y puerto
                //JtdsConnection validateConnection = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,usr,pass);

                //******************************************  LOGIN ADMIN *******************************************************************
                //usr
                //pass


                cnDest = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,"UsrS","753xul_");
                Statement stmLogin = cnDest.createStatement();


                /*  HABILITAR ANTES DE LIBERAR

                rst = stmLogin.executeQuery("select * from ADMM_USUARIOS where cve_usuario = '" + usrSICSEJ.toUpperCase().trim() + "'");
                Result result = ResultSupport.toResult(rst);
                int rowcount = result.getRowCount( );
                if (rowcount == 0){

                    errorMessage("¡Error!","El Usuario no existe en ADMM_USUARIOS favor de verificarlo, insertelo y asignele los permisos correspondientes");
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }


                 */


                //cnAdmin = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,"usr","pass");
                //Statement stmAdmin = cnAdmin.createStatement();

                rst = stmLogin.executeQuery("select * from sys.sql_logins where name = '" + usrSICSEJ.toUpperCase().trim() + "'");
                Result result = ResultSupport.toResult(rst);
                int rowcount = result.getRowCount( );
                if (rowcount == 0 ){
                    loginExist = 0;
                    rowcount = 0;
                }else{
                    loginExist = 1;
                    rowcount = 0;
                }


                rst = stmLogin.executeQuery("select * from sysusers where name = '" + usrSICSEJ.toUpperCase().trim() + "'");
                result = ResultSupport.toResult(rst);
                rowcount = result.getRowCount( );
                if (rowcount == 0 ){
                    userExist = 0;
                    rowcount = 0;
                }else{
                    userExist = 1;
                    rowcount = 0;
                }


                if (loginExist == 1 && userExist == 1){
                    //changePassOnly();
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }
                if (loginExist == 1 && userExist == 0){

                    PrimeFaces.current().executeScript("PF('confirmDialogwidgetUser').show();");
                    //createUserChangePass();
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }
                if (loginExist == 0 && userExist == 1){
                    //confirmDialogwidgetLogin
                    //PrimeFaces.current().executeScript("PF('confirmDialogwidget').show();");
                    PrimeFaces.current().executeScript("PF('confirmDialogwidgetLogin').show();");
                    //createLoginChangePass();
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }
                if (loginExist == 0 && userExist == 0){
                    PrimeFaces.current().executeScript("PF('confirmDialogwidgetLoginPass').show();");
                    //createLoginUserChangePass();
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }






                if (rowcount == 0){
                    //warnMessage("¡Error!","No existe el inicio de sesión:" + usrSICSEJ.toUpperCase().trim() + " ,pero se GENERARÁ" );
                    //PrimeFaces.current().executeScript("alert('hola');");
                    PrimeFaces.current().executeScript("PF('confirmDialogwidget').show();");


                    //System.out.println("Se ejecutará:" + "exec sp_addlogin '" + usrSICSEJ.toUpperCase().trim() + "','" + passUsrNew.trim() + "','" + bd + "','Español'");

                    //String sqlInstruction = "sp_addlogin " + usrSICSEJ.trim() + ",'" + passUsrNew.trim() + ",'" + bd + "','Español''";
                    //System.out.println(sqlInstruction);

                    //stm.execute("exec sp_addlogin '" + usrSICSEJ.toUpperCase().trim() + "','" + passUsrNew.trim() + "','" + bd + "','Español'");
                  /*  stmLogin.execute("CREATE LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH PASSWORD = '" + passUsrNew.trim() + "'");
                    stmLogin.execute("CREATE USER  " + usrSICSEJ.toUpperCase().trim() + " FOR LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH DEFAULT_SCHEMA = dbo");
                    stmLogin.execute("EXEC sp_addrolemember 'desarrollo','" + usrSICSEJ.toUpperCase().trim() + "'");
                    stmLogin.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() + 40 WHERE cve_usuario = '" + usrSICSEJ  + "'");*/
                    //exec sp_adduser @usr, @usr, 'desarrollo'
                    //stm.execute("exec sp_adduser '" + usrSICSEJ.toUpperCase().trim() + "','desarrollo'");

                    //newUserCreated = 1;

                  /*  successMessage("¡Exito!", "El inicio de sesión fue creado para la Base de Datos seleccionada y su contraseña se asignó");
                    warnMessage("¡NOTA!","El inicio de sesión:" + usrSICSEJ.toUpperCase().trim() + " ,fue creado, pero puede que no tenga permisos en SICSEJ (Modulos), Verifiquelo por favor" );
*/
                    return;

                }




                Statement stmChangePass = cnDest.createStatement();
                //exec sp_addlogin @usr, 'temporal01*', @bdsicj, 'Español'
                //exec sp_adduser @usr, @usr, 'desarrollo'


                stmChangePass.execute("ALTER LOGIN " + usrSICSEJ.trim() + " WITH PASSWORD = '" + passUsrNew.trim() + "'");
                stmLogin.execute("ALTER LOGIN [" + usrSICSEJ.trim() + "] WITH DEFAULT_DATABASE =[" + bd + "]");
                stmChangePass.execute("ALTER USER " + usrSICSEJ.trim() + " WITH DEFAULT_SCHEMA = dbo");
                stmLogin.execute("EXEC sp_addrolemember 'desarrollo','" + usrSICSEJ.toUpperCase().trim() + "'");
                //stmLogin.execute("ALTER LOGIN [" + usrSICSEJ.trim() + "] WITH DEFAULT_DATABASE =[" + bd + "]");
                stmChangePass.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() + 40 WHERE cve_usuario = '" + usrSICSEJ  + "'");


                //if (newUserCreated == 0) {
                successMessage("¡Exito!", "Contraseña cambiada exitosamente");
                //return;
                //}
                /*else if(newUserCreated == 1){
                    successMessage("¡Exito!", "El inicio de sesión fue creado para la Base de Datos seleccionada y su contraseña se asignó");
                    warnMessage("¡NOTA!","El inicio de sesión:" + usrSICSEJ.toUpperCase().trim() + " ,fue creado, pero puede que no tenga permisos en SICSEJ (Modulos), Verifiquelo por favor" );
                }*/


                PrimeFaces.current().resetInputs("form:pnlFields");

            }
        }
        catch(SQLException e){
            //aqui cachar el error de conexión y aqui mismo mandar el growl
            System.out.println("err: "+e.getMessage());
            //errorMessage("¡Error!","Usuario y contraseña invalidos");

            if (e.getErrorCode() == 15032) {
                try {

                    cnDest = CreateDataSource.getConnParam(ip, Integer.parseInt(port), bd, "UsrS", "753xul_");
                    Statement stmLogin = cnDest.createStatement();

                    stmLogin.execute("ALTER LOGIN [" + usrSICSEJ.trim() + "] WITH PASSWORD = '" + passUsrNew.trim() + "'");
                    stmAdmin.execute("EXEC sp_addrolemember 'desarrollo','" + usrSICSEJ.toUpperCase().trim() + "'");
                    //ALTER LOGIN [my_user_name] WITH DEFAULT_DATABASE = [new_default_database]
                    stmLogin.execute("ALTER LOGIN [" + usrSICSEJ.trim() + "] WITH DEFAULT_DATABASE =[" + bd + "]");

                    stmLogin.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() + 40 WHERE cve_usuario = '" + usrSICSEJ + "'");

                    cnDest = CreateDataSource.getConnParam(ip, Integer.parseInt(port), bd, "UsrS", "753xul_");
                    System.out.println("cnDest: " + cnDest);

                    ResultSet rst = stmLogin.executeQuery("select * from sys.sql_logins where name = '" + usrSICSEJ.toUpperCase().trim() + "'");
                }
                catch(Exception ex){
                    errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + String.valueOf(e.getErrorCode()));
                }


            }


            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + String.valueOf(e.getErrorCode()));
        }
        catch(Exception e){
            //aqui cachar el error de conexión y aqui mismo mandar el growl
            System.out.println("err: "+e.getMessage());
            //errorMessage("¡Error!","Usuario y contraseña invalidos");
            errorMessage("¡Error!","Usuario y contraseña invalidos");
        }

    }


    public String test() {
        System.out.println("test");
        return "";
    }


    public void initCounters(){
        System.out.println("si");
            try {

                //cnInit = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,usr,pass);

                cnInit = CreateDataSource.getConnParam("sicsj-ss.tfja.gob.mx",Integer.parseInt("1433"),"BDSICJ-RF","sa","1Qazxsw2");
                Statement stmCounter = cnInit.createStatement();
                ResultSet rst = stmCounter.executeQuery("exec sp_init_counters");

            }
            catch (Exception e){

            }

        }




        public void successMessage(String summary,String message) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,summary,message ) );
        }
        public void errorMessage(String summary,String message) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,summary,message ) );
        }
        public void warnMessage(String summary,String message) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,summary,message ) );
        }



        public void dinamycDialog(String summary,String message) {

            FacesContext.getCurrentInstance().


                    addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,summary,message ) );
        }



        public String getUsr() {
            return usr;
        }

        public void setUsr(String usr) {
            this.usr = usr;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getBase() {
            return base;
        }

        public void setBase(String base) {
            this.base = base;
        }
    }





