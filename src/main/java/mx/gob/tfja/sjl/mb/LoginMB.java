package mx.gob.tfja.sjl.mb;

import mx.gob.tfja.sjl.data.CreateDataSource;
import net.sourceforge.jtds.jdbc.JtdsConnection;
import org.primefaces.PrimeFaces;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.sql.ResultSet;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;
import java.sql.SQLException;
import java.sql.Statement;
import mx.gob.tfja.sjl.util.SessionUtils;
import java.util.concurrent.TimeUnit;



@ManagedBean(name = "loginMB")
@SessionScoped

public class LoginMB implements Serializable{
    private String usr;
    private String pass;

    private String base;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    private String region;
    private String bd;
    private String ip;
    private String port;
    public String[] arrOfStr;

    private static JtdsConnection cnAdmin, cnAutentica;
    private static Statement stmAdmin;

    public String autenticate() {
        usr = usr.toUpperCase();

        if (region != null && !region.trim().isEmpty()) {
            //ResultSet rst = stm.executeQuery("select ip_server,nom_bd from CLAC_SALAS where descripcion = '" + region + "'");
            //CAT_BD_REGION_CLOUD
            arrOfStr = region.split("_", 3);
            try {


                cnAdmin = CreateDataSource.getConnection();
                stmAdmin = cnAdmin.createStatement();
                ResultSet rst = stmAdmin.executeQuery("select ref_sec,dns,ip_server_port from CLAC_SALAS where descripcion = '" + arrOfStr[0] + "'");

                while (rst.next()) {
                    ip = rst.getString(2);
                    bd = rst.getString(1);
                    port = rst.getString(3);
                    //usr = rst.getString(4);
                    //pass = rst.getString(5);
                }

                //  Valida que el usuario exista en la BD
                try {
                    cnAutentica = CreateDataSource.getConnParam(ip, Integer.parseInt(port), bd, usr, pass);
                    HttpSession session = SessionUtils.getSession();
                    session.setAttribute("username", usr);
                    session.setAttribute("regConnection", cnAutentica);
                    session.setAttribute("ip", ip);
                    session.setAttribute("port", port);
                    session.setAttribute("bd", bd);

                    successMessage("¡Bienvenido Administrador ! Base: " + bd + " Seleccione una opción del menú lateral izquierdo ", "Usuario Logueado: " + usr);



                }catch (SQLException e){
                    //System.out.println(e.getMessage());
                    if (e.getErrorCode() == 18456){
                        errorMessage("¡Error!","Usuario o contraseña invalidos");
                        UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
                        return view.getViewId() + "?faces-redirect=true";
                    }


                    System.out.println(e.getErrorCode());
                }

                // Redireccionar a Menú Central
                //redirectionTFJA(1);

                //UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
                //redirectionTFJA(view.getViewId() + "?faces-redirect=true");

                /*UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
                System.out.println("view: "+view);
                System.out.println("view: "+view.getViewId());
                refreshForm(view.getViewId() + "?faces-redirect=true");
*/

            } catch (Exception e) {
                e.getMessage();
            }
        }


        return ("menuCentral");
        //return ("");

    }

    public String logout(){
        HttpSession session = SessionUtils.getSession();
        session.invalidate();
        successMessage("¡ Sesión concluida !", "Termina Sesión");
        return "login";
    }


    public void successMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,summary,message ) );
    }
    public void errorMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,summary,message ) );
    }
    public void warnMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,summary,message ) );
    }

    //public String logout() {
    //    HttpSession session = SessionUtils.getSession();
    //    session.invalidate();
    //    return "logout";
    //}


   public String refreshForm(String xPage){
        return xPage;
   }


    public String redirectionTFJA(int xPage){
        //System.out.println("Por redireccionar");


        if(SessionUtils.getUserName() != null && SessionUtils.getUserName() != null) {
            if (xPage == 0) {
                System.out.println("0");
                return "changePass";
            }
            if (xPage == 1) {
                System.out.println("1");
                return "changePass";
            }
            if (xPage == 2) {
                System.out.println("3");
                return "startCounter";
            }

        }
        else {
            return "login";
        }
        return "login";
    }


    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
