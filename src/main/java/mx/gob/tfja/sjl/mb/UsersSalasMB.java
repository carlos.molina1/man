


package mx.gob.tfja.sjl.mb;


import mx.gob.tfja.sjl.data.CreateDataSource;
import mx.gob.tfja.sjl.dto.UserSalas;

import mx.gob.tfja.sjl.dto.Users;

import mx.gob.tfja.sjl.util.SessionUtils;
import net.sourceforge.jtds.jdbc.JtdsConnection;
import org.primefaces.event.RowEditEvent;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ManagedBean(name = "usersSalasMB")
@SessionScoped

public class UsersSalasMB implements Serializable {
    private static JtdsConnection connection;
    private List<UserSalas> userSalas, allSalas;
    private List<String> salas;//,allSalas;
    private UserSalas sala, newGate;
    private Users user;
    private List<Users> users;
    private Map<Integer, UserSalas> map;
    private int lastIndex;

    @PostConstruct
    public void init() {
        try {
            System.out.println("init");
            HttpSession session = SessionUtils.getSession();

            connection = (JtdsConnection) session.getAttribute("regConnection");
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("Select cve_usuario,ap_paterno,ap_materno,nombre from ADMM_USUARIOS");
            users = new ArrayList<>();
            //String key,String lastName, String surname, String firstName )
            while (rs.next()) {
                users.add(new Users(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void onUserSelect() {
        // System.out.println("sala: "+sala);
        try {
            HttpSession session = SessionUtils.getSession();
            connection = (JtdsConnection) session.getAttribute("regConnection");
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("Select LTRIM(RTRIM(cve_usuario)),cve_region, cve_sala, cve_mag, cve_srio " +
                    "from ADMR_USUARIOS_SALAS where cve_usuario = '" + user.getKey() + "' ORDER BY cve_region, cve_sala, cve_mag, cve_srio");

            userSalas = new ArrayList<>();


            //String key,String lastName, String surname, String firstName )
            map = new HashMap<>();
            int c = 1;
            while (rs.next()) {
                //salas.add(rs.getString(1));
                /*
                ,Integer.parseInt(rs.getString(2)),Integer.parseInt(rs.getString(3)),
                        Integer.parseInt(rs.getString(4)),Integer.parseInt(rs.getString(5)

                        (String cveUser, int cveRegion, int cveSala, int cveMag, int cveSrio)
                 */
                UserSalas u = new UserSalas(c, rs.getString(1), Integer.parseInt(rs.getString(2)), Integer.parseInt(rs.getString(3)),
                        Integer.parseInt(rs.getString(4)), Integer.parseInt(rs.getString(5)));
                map.put(c, u);
                userSalas.add(u);
                if (c < 3) {
                    System.out.println("s: " + u);
                    System.out.println("===================");
                }
                c++;
                lastIndex = c;
            }
            System.out.println("salas: " + userSalas.size());

            //
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void onRowEdit(RowEditEvent e) {
        UserSalas u = e.getObject() != null ? (UserSalas) e.getObject() : null;
        if (u != null) {
            //(String cveUser, int cveRegion, int cveSala, int cveMag, int cveSrio)
            System.out.println("id: " + u.getId());
            UserSalas um = map.containsKey(u.getId()) ? map.get(u.getId()) : null;
            System.out.println("um: " + um);
            if (um != null) {
                um = new UserSalas(u.getId(), u.getCveUser(), u.getCveRegion(), u.getCveSala(), u.getCveMag(), u.getCveSrio());
                map.put(u.getId(), um);
            }
            System.out.println("um editado: " + um);
        }

    }

    public void saveChanges() {
        try {

            HttpSession session = SessionUtils.getSession();


            System.out.println("salas: " + userSalas.size());
            System.out.println("allSalas: " + allSalas.size());

            List<String> salasL = new ArrayList<>(salas.size());
            userSalas.forEach(s -> {
                salasL.add(new StringBuilder("[").append(String.valueOf(s.getCveRegion())).append(
                        String.valueOf(s.getCveSala())).append(
                        String.valueOf(s.getCveMag())).append(
                        String.valueOf(s.getCveSrio())).append(']').toString());

            });
            System.out.println("modulesL: " + salasL);
            System.out.println("===========================");


            /*

            try {
                StringBuilder sb= new StringBuilder("");
                Statement statement = connection.createStatement();

                boolean b = statement.execute(" BEGIN TRAN MAN_MOD_1_V5_ALFA");

                sb.append(" \n ");
                sb.append(" delete from ADMR_USUARIOS_MODULOS where cve_usuario = '").append(user.getKey().trim()).append("'");
                sb.append(" \n ");
                sb.append(" INSERT INTO ADMR_USUARIOS_MODULOS (cve_usuario, cve_modulo, modificar, restrictiva) ");
                sb.append(" \n ");
                sb.append(" Select '").append(user.getKey().trim()).append("',cve_modulo,0,0 from ADMM_MODULOS where  cve_modulo in (");
                String modulesS=modulesL.toString();
                modulesS=modulesS.substring(1,modulesS.length()-1);
                System.out.println("modulesS: "+modulesS);
                sb.append(modulesS);
                sb.append(")");

                //sb.append(" \n ").append(" COMMIT TRAN T1 ").append(" \n ").append(" ROLLBACK TRAN T1 ");
                System.out.println("query: "+sb.toString());
                connection = (JtdsConnection) session.getAttribute("regConnection");



                statement = connection.createStatement();
                b = statement.execute(sb.toString());
                System.out.println("b " + b);

                statement = connection.createStatement();
                b = statement.execute(" COMMIT TRAN MAN_MOD_1_V5_ALFA ");

                System.out.println("Se realizó el cambio de módulos satisfactoriamente ");
                System.out.println("b " + b);


                //boolean b = statement.execute(" BEGIN TRAN T1 ");

                successMessage("¡ Módulos asignados/modificados satisfactoriamente ! ", "Asignación/Cambio Módulos");


            }
            catch (Exception e){
                Statement statement = connection.createStatement();
                boolean b = statement.execute(" ROLLBACK TRAN MAN_MOD_1_V5_ALFA ");
                System.out.println("Ocurrió un error al intentar cambiar los módulos ");
                errorMessage("Error en la asignación de Módulos " + e.getMessage() + " \n ","Error Asignación/Modificación Módulos");
                System.out.println("Error en la asignación de Módulos " + e.getMessage() + " \n ");
                return;
            }


        }

             */


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createNewGate(boolean init) {
        newGate = init ? new UserSalas() : null;
    }
    public void saveAll(){
        try {

            HttpSession session = SessionUtils.getSession();

            try {
                StringBuilder sb= new StringBuilder("");
                Statement statement = connection.createStatement();

                boolean b = statement.execute(" BEGIN TRAN MAN_MOD_1_V5_ALFA");

                sb.append(" \n ");
                sb.append(" delete from ADMR_USUARIOS_SALAS where cve_usuario = '").append(user.getKey().trim()).append("'");
                for(UserSalas u:userSalas){
                    sb.append(" \n ");
                    sb.append(" INSERT INTO ADMR_USUARIOS_SALAS (cve_usuario, cve_region, cve_sala, cve_mag, cve_srio) ");
                    sb.append(" \n ");
                    sb.append(" values (");
                    sb.append("'").append(u.getCveUser().trim()).append("',").append(u.getCveRegion()).append(",")
                            .append(u.getCveSala()).append(",").append(u.getCveMag()).append(",")
                            .append(u.getCveSrio()).append(")");
                }




                //sb.append(" \n ").append(" COMMIT TRAN T1 ").append(" \n ").append(" ROLLBACK TRAN T1 ");
                System.out.println("query: "+sb.toString());
                connection = (JtdsConnection) session.getAttribute("regConnection");



                statement = connection.createStatement();
                b = statement.execute(sb.toString());
                System.out.println("b " + b);

                statement = connection.createStatement();
                b = statement.execute(" COMMIT TRAN MAN_MOD_1_V5_ALFA ");

                System.out.println("Se realizó el cambio de salas satisfactoriamente ");
                System.out.println("b " + b);


                //boolean b = statement.execute(" BEGIN TRAN T1 ");

                successMessage("¡ Salas asignadas/modificadas satisfactoriamente ! ", "Asignación/Cambio Salas");


            }
            catch (Exception e){
                Statement statement = connection.createStatement();
                boolean b = statement.execute(" ROLLBACK TRAN MAN_MOD_1_V5_ALFA ");
                System.out.println("Ocurrió un error al intentar cambiar los módulos ");
                errorMessage("Error en la asignación de Salas " + e.getMessage() + " \n ","Error Asignación/Modificación Salas");
                System.out.println("Error en la asignación de Salas " + e.getMessage() + " \n ");
                return;
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void saveGate() {
        lastIndex++;
        newGate.setId(lastIndex);
        newGate.setCveUser(user.getKey().trim());
        System.out.println("newGate: " + newGate);
        map.put(lastIndex, newGate);
        userSalas.add(0, newGate);
        createNewGate(Boolean.FALSE);
        successMessage("Exito!","Registro agregado");
    }

    public void successMessage(String summary, String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, message));
    }

    public void errorMessage(String summary, String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, message));
    }

    public void warnMessage(String summary, String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, message));
    }


    public List<UserSalas> getUserSalas() {
        return userSalas;
    }

    public void setUserSalas(List<UserSalas> userSalas) {
        this.userSalas = userSalas;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public UserSalas getNewGate() {
        return newGate;
    }

    public void setNewGate(UserSalas newGate) {
        this.newGate = newGate;
    }
}
