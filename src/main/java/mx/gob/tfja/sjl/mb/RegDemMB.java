package mx.gob.tfja.sjl.mb;
import mx.gob.tfja.sjl.util.SessionUtils;
import net.sourceforge.jtds.jdbc.JtdsConnection;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import mx.gob.tfja.sjl.data.CreateDataSource;
import mx.gob.tfja.sjl.data.SqlDataSource;
import org.primefaces.PrimeFaces;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;

@ManagedBean(name = "comRegDemanda")
@ViewScoped
public class RegDemMB  implements Serializable {

    private static JtdsConnection cn;
    private String regionDescription;
    private String region;
    private String usr;
    private String pass;
    private String base;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    private JtdsConnection initialCn;

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public List<String> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<String> regionList) {
        this.regionList = regionList;
    }

    private List<String> regionList;


    //public RegDemMB() {
//        regionList = new HashMap<String, String>();
  //  }

    //public String getRegionDescription() {
    //  return regionDescription;
    //}
    public void setRegionDescription(String regionDescription) {
        this.regionDescription = regionDescription;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }


    @PostConstruct
    public void init() {

        initialCn=CreateDataSource.getConnection();
        System.out.println("initialCn PostConstruct: "+initialCn);
        getRegionDesc();
        PrimeFaces.current().resetInputs(usr);
        PrimeFaces.current().resetInputs(pass);
        //PrimeFaces.current().resetInputs("form:pnlFields");
    }



    public void getRegionDesc() {
        try {
            regionList= new ArrayList<>();

            if (initialCn != null) {
                System.out.println("Conectado");
               Statement stm = initialCn.createStatement();
                System.out.println("stm: "+stm);
                ResultSet rst = stm.executeQuery("Select RTRIM(descripcion) + '_' + ref_sec  from CLAC_SALAS");
                //ResultSet rst = stm.executeQuery("Select RTRIM(descripcion) from CLAC_SALAS");

                while (rst.next()) {
                    regionList.add( rst.getString(1));
                }
            } else {
                System.out.println("Desconectado");
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("regionList: "+regionList.size());
    }


    public String redirectionTFJA(int xPage){
        //System.out.println("Por redireccionar");
        if (xPage == 0) {
            return "menuCentral";
        }
        if (xPage == 1) {
            return "changePass";
        }
        if (xPage == 2) {
            return "usersPermissions";
        }

        if (xPage == 3) {
            return "usersSalas";
        }

        if (xPage == 4) {
            return "startCounter";
        }

        if (xPage == 5) {
            return "assignGuard";
        }

        //HttpSession session = SessionUtils.getSession().invalidate();
        //session.setAttribute("username", usr);
        //session.setAttribute("regConnection", cnAutentica);
        //session.setAttribute("ip", ip);
        //session.setAttribute("port", port);
        //session.setAttribute("bd", bd);


        /*
        if (xPage == 3) {
            return "assignGuard";
        }
        */

        return "startCounter";
    }






    /*
    private void getConection() {
        try {
            Class.forName("net.sourceforge.jtds.jdbc.JtdsConnection");
            cn = (JtdsConnection) DriverManager.getConnection("jdbc:jtds:sqlserver://172.16.16.4:1433;databaseName=BDSICJ", "entsis", "entsis");
        } catch (Exception e) {
            cn = null;
        }
    }

     */

    //------------------------------------------------------------------------------------



    /*
    public List<String> getRegionDescription() {
        try {
            JdbcTemplate jdbc = new JdbcTemplate(SqlDataSource.mSQLdataSource());
            String query = new String("select * from CLAC_SALAS");

            List<Map> rows = getJdbcTemplate().queryForList(sql);

            //regionList = jdbc.queryForList("select region from CLAC_SALAS", new Object[]{});


                    //queryForList("select region from CLAC_SALAS", new Object[]{}, String.class);

        } catch (Exception e) {
            System.out.println(e);
        }
        //return regionList;
    }


     */

}



