package mx.gob.tfja.sjl.mb;

import mx.gob.tfja.sjl.data.CreateDataSource;
import mx.gob.tfja.sjl.util.SessionUtils;
import net.sourceforge.jtds.jdbc.JtdsConnection;
import org.primefaces.PrimeFaces;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.sql.ResultSet;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;
import java.sql.SQLException;
import java.sql.Statement;
//import org.springframework.jdbc.core.RowMapper;

@ManagedBean(name = "searchMB")
@ViewScoped
public class SearchMB implements Serializable {

    private static JtdsConnection cn;
    private static JtdsConnection cnDest;
    private static JtdsConnection cnAdmin;
    private static Statement stmAdmin;
    private String usr;
    private String pass;
    private String base;
    private String region;
    private String bd;
    private String ip;
    private String port;
    private String usrSICSEJ;
    private String passUsrNew;
    public String[] arrOfStr;
    public int newUserCreated = 0;
    public int userExist = 0;
    public int loginExist = 0;


    public String getUsrSICSEJ() {
        return usrSICSEJ;
    }

    public void setUsrSICSEJ(String usrSICSEJ) {
        this.usrSICSEJ = usrSICSEJ;
    }

    public String getPassUsrNew() {
        return passUsrNew;
    }

    public void setPassUsrNew(String passUsrNew) {
        this.passUsrNew = passUsrNew;
    }





    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public void cnSearch()
    {
        try {

            //Obtener los valores de sesión


            HttpSession session = SessionUtils.getSession();
            //session.setAttribute("username", usr);
            //session.setAttribute("regConnection", cnAutentica);

            //Object userLogIn;


            Object userLogIn = session.getAttribute("username");
            Object cnRegional = session.getAttribute("regConnection");
            Object objIp = session.getAttribute("ip");
            Object objPort = session.getAttribute("port");
            Object objBD = session.getAttribute("bd");

            String strIp = objIp.toString();
            String strPort = objPort.toString();
            String strBD = objBD.toString();


            if (cnRegional == null)
            {
                errorMessage("¡Error!","Debe loguearse primero para poder ingresar al Sistema");
                return;
            }

            String usrAdmin = userLogIn.toString();
            JtdsConnection cnAdmin;
            cnAdmin = (JtdsConnection) cnRegional;




            //usr="ADMJMOLINA";
            //pass = "jcmc09./*-";
            if (usrAdmin.toUpperCase().indexOf("ADMIN") == -1 && usrAdmin.toUpperCase().indexOf("ADM") == -1 ){
                errorMessage("¡Error!","Solo un usuario administrador puede cambiar contraseñas");
                return;
            }
            if (usrSICSEJ.toUpperCase().indexOf("VISITA") >= 0){
                errorMessage("¡Error!","No puede cambiar la contraseña a usuarios de VISITA");
                return;
            }
            if (usrSICSEJ.toUpperCase().indexOf("CONS") >= 0){
                errorMessage("¡Error!","No puede cambiar la contraseña a usuarios de CONSULTA");
                return;
            }


                cnDest = CreateDataSource.getConnParam(strIp, Integer.parseInt(strPort), strBD, "UsrS", "753xul_");
                Statement stmLogin = cnDest.createStatement();


                ResultSet rst = stmLogin.executeQuery("select * from sys.sql_logins where name = '" + usrSICSEJ.toUpperCase().trim() + "'");
                Result result = ResultSupport.toResult(rst);
                int rowcount = result.getRowCount();
                if (rowcount == 0) {
                    loginExist = 0;
                    rowcount = 0;
                } else {
                    loginExist = 1;
                    rowcount = 0;
                }

                rst = stmLogin.executeQuery("select * from sysusers where name = '" + usrSICSEJ.toUpperCase().trim() + "'");
                result = ResultSupport.toResult(rst);
                rowcount = result.getRowCount( );
                if (rowcount == 0 ){
                    userExist = 0;
                    rowcount = 0;
                }else{
                    userExist = 1;
                    rowcount = 0;
                }


                if (loginExist == 1 && userExist == 1){
                    //changePassOnly();


                    cnDest = CreateDataSource.getConnParam(strIp,Integer.parseInt(strPort),strBD,"UsrS","753xul_");
                    Statement stmLoginCP = cnDest.createStatement();
                    stmLoginCP.execute("ALTER LOGIN [" + usrSICSEJ.trim() + "] WITH DEFAULT_DATABASE =[" + strBD + "]");
                    stmLoginCP.execute("ALTER LOGIN [" + usrSICSEJ.trim() + "] WITH PASSWORD = '" + passUsrNew + "'");
                    stmLoginCP.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() - 1 WHERE cve_usuario = '" + usrSICSEJ + "'");
                    successMessage("¡Exito!", "Contraseña cambiada exitosamente");


                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }
                if (loginExist == 1 && userExist == 0){

                    PrimeFaces.current().executeScript("PF('cdUser').show();");
                    //createUserChangePass();
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }
                if (loginExist == 0 && userExist == 1){
                    //confirmDialogwidgetLogin
                    //PrimeFaces.current().executeScript("PF('confirmDialogwidget').show();");
                    PrimeFaces.current().executeScript("PF('cdLogin').show();");
                    //createLoginChangePass();
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }
                if (loginExist == 0 && userExist == 0){
                    PrimeFaces.current().executeScript("PF('cdLoginPass').show();");
                    //createLoginUserChangePass();
                    PrimeFaces.current().resetInputs("form:pnlFields");
                    return;
                }

        }
        catch(SQLException e){
            //aqui cachar el error de conexión y aqui mismo mandar el growl
            System.out.println("err: "+e.getMessage());
            //errorMessage("¡Error!","Usuario y contraseña invalidos");

            if (e.getErrorCode() == 15032) {

            }
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + String.valueOf(e.getErrorCode()));
        }
        catch(Exception e){
            e.printStackTrace();
            //aqui cachar el error de conexión y aqui mismo mandar el growl
            System.out.println("err: "+e.getMessage());
            //errorMessage("¡Error!","Usuario y contraseña invalidos");
            errorMessage("¡Error!","Error de sesion " +  e.getMessage());
        }

    }

    public void changePassOnly(){
        try {
            cnDest = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,"UsrS","753xul_");
            Statement stmLogin = cnDest.createStatement();
            stmLogin.execute("ALTER LOGIN [" + usrSICSEJ.trim() + "] WITH DEFAULT_DATABASE =[" + bd + "]");
            stmLogin.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() - 1 WHERE cve_usuario = '" + usrSICSEJ + "'");
            successMessage("¡Exito!", "Contraseña cambiada exitosamente");
        }
        catch(SQLException e){
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + String.valueOf(e.getErrorCode()));
        }
        catch(Exception e){
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + e.getMessage());
        }
    }

    public void createUserChangePass(){
        try {
            cnDest = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,"UsrS","753xul_");
            Statement stmLogin = cnDest.createStatement();
            stmLogin.execute("CREATE USER  " + usrSICSEJ.toUpperCase().trim() + " FOR LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH DEFAULT_SCHEMA = dbo");
            stmLogin.execute("EXEC sp_addrolemember 'desarrollo','" + usrSICSEJ.toUpperCase().trim() + "'");
            stmLogin.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() - 1 WHERE cve_usuario = '" + usrSICSEJ  + "'");
            successMessage("¡Exito!", "Usuario creado exitosamente");
            successMessage("¡Exito!", "Contraseña cambiada exitosamente");

        }
        catch(SQLException e){
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + String.valueOf(e.getErrorCode()));
        }
        catch(Exception e){
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + e.getMessage());
        }
    }


    public void createLoginChangePass(){
        try {
            cnDest = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,"UsrS","753xul_");
            Statement stmLogin = cnDest.createStatement();
            stmLogin.execute("CREATE LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH PASSWORD = '" + passUsrNew.trim() + "'");
            stmLogin.execute("EXEC sp_addrolemember 'desarrollo','" + usrSICSEJ.toUpperCase().trim() + "'");
            stmLogin.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() - 1 WHERE cve_usuario = '" + usrSICSEJ  + "'");
            successMessage("¡Exito!", "Inicio de sesión creado exitosamente");
            successMessage("¡Exito!", "Contraseña cambiada exitosamente");
            warnMessage("¡NOTA!","El inicio de sesión:" + usrSICSEJ.toUpperCase().trim() + " ,fue creado, pero puede que no tenga permisos en SICSEJ (Modulos), Verifiquelo por favor" );
        }
        catch(SQLException e){
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + String.valueOf(e.getErrorCode()));
        }
        catch(Exception e){
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + e.getMessage());
        }
    }


    public void createLoginUserChangePass(){
        System.out.println("createLoginUserChangePass");
        try {
            cnDest = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,"UsrS","753xul_");
            Statement stmLogin = cnDest.createStatement();
            stmLogin.execute("CREATE LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH PASSWORD = '" + passUsrNew.trim() + "'");
            stmLogin.execute("CREATE USER  " + usrSICSEJ.toUpperCase().trim() + " FOR LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH DEFAULT_SCHEMA = dbo");
            stmLogin.execute("EXEC sp_addrolemember 'desarrollo','" + usrSICSEJ.toUpperCase().trim() + "'");
            stmLogin.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE() - 1 WHERE cve_usuario = '" + usrSICSEJ  + "'");
            PrimeFaces.current().executeScript("PF('cdLoginPass').hide();");
            System.out.println("dialogo cerrado");
            successMessage("¡Exito!", "Inicio de sesión creado exitosamente");

            successMessage("¡Exito!", "Usuario creado exitosamente");
        }
        catch(SQLException e){
            e.printStackTrace();
            errorMessage("¡Error!",e.getMessage() + " " + " Código de ERROR:" + String.valueOf(e.getErrorCode()));
        }
        catch(Exception e){
e.printStackTrace();
        }
    }






    public void confirmUserCreation(){
        System.out.println("confirmUserCreation");
        System.out.println("ip: "+ip);
        System.out.println("port: "+port);
        System.out.println("bd: "+bd);
        System.out.println("usrSICSEJ.toUpperCase().trim(): "+usrSICSEJ.toUpperCase().trim());
        System.out.println("passUsrNew.trim(): "+passUsrNew.trim());

        try {
            cnDest = CreateDataSource.getConnParam(ip,Integer.parseInt(port),bd,"UsrS","753xul_");
            Statement stmLogin = cnDest.createStatement();
            stmLogin.execute("CREATE LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH PASSWORD = '" + passUsrNew.trim() + "'");
            stmLogin.execute("CREATE USER  " + usrSICSEJ.toUpperCase().trim() + " FOR LOGIN " + usrSICSEJ.toUpperCase().trim() + " WITH DEFAULT_SCHEMA = dbo");
            stmLogin.execute("EXEC sp_addrolemember 'desarrollo','" + usrSICSEJ.toUpperCase().trim() + "'");
            stmLogin.execute("UPDATE ADMM_USUARIOS SET dia_caduc = GETDATE()-1 WHERE cve_usuario = '" + usrSICSEJ  + "'");
            //exec sp_adduser @usr, @usr, 'desarrollo'
            //stm.execute("exec sp_adduser '" + usrSICSEJ.toUpperCase().trim() + "','desarrollo'");

            newUserCreated = 1;
            //exec sp_addlogin @usr, 'temporal01*', @bdsicj, 'Español'
            //exec sp_adduser @usr, @usr, 'desarrollo'

            PrimeFaces.current().executeScript("PF('confirmDialogwidget').hide();");
            successMessage("¡Exito!", "El inicio de sesión fue creado para la Base de Datos seleccionada y su contraseña se asignó");
            warnMessage("¡NOTA!","El inicio de sesión:" + usrSICSEJ.toUpperCase().trim() + " ,fue creado, pero puede que no tenga permisos en SICSEJ (Modulos), Verifiquelo por favor" );
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public void successMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,summary,message ) );
    }
    public void errorMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,summary,message ) );
    }
    public void warnMessage(String summary,String message) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,summary,message ) );
    }



    public void dinamycDialog(String summary,String message) {

        FacesContext.getCurrentInstance().


                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,summary,message ) );
    }



    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
